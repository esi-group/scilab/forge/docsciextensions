Documentation : Writing Scilab Extensions

Abstract

In this document, we present methods to extend Scilab 
features.
In the first part, we focus on external modules. We describe their 
general organization and how to install a module from ATOMS. 
Then we describe how to build a module from the sources.
In the second part, we focus on interfaces, which allows to 
connect Scilab to a compiled library. We consider the example
of a simple function in the C language and explore several ways to make this 
function available to Scilab. We consider a simple method based on
exchanging data by file. A more elaborate method is based on the 
call function. Finally, we present how to design a gateway
and how to use the Scilab API.

Author

Copyright (C) 2012 - Michael Baudin

Licence

This document is released under the terms of the Creative Commons Attribution-ShareAlike 3.0 Unported License :
http://creativecommons.org/licenses/by-sa/3.0/

TODO
 * Check the spelling of the document with a software.
 * Add a wrap-up section: the structure of a template module with macros
 * Add a wrap-up section: the structure of a template module with a source code
